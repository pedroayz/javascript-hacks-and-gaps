//11 - Bucles - 'While' & 'doWhile'
//Bulces determinados eh indeterminados (detrimanda o no la cantidad de veces que el bucle tendra que ejecutarse)

/* Sintaxis general 

bucle {

    codigo()

}

*/

// Bucle 'While' & 'doWhile'
//Bulce indeterminado
/* 
    while(){

        codigo()
    }
----------------------------
    do {

     } while()
-----------------------------
*/

let pass = '';
// WHILE
while(pass != 'hola'){

   pass = prompt('Introduce tu Contraseña')
}

console.log('Fin Del Bucle While')

//DO wHILE
do{

    pass = prompt('Introduce tu Contraseña')

}
while(pass != 'hola')

console.log('Fin Del Bucle DO WHILE')

