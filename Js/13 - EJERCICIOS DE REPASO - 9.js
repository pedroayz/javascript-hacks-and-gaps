/* 9 - Solicitar al usuario una palabra y mostrar por consola el número de
 consonantes, vocales y longitud de la palabra.
*/

const letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];


let palabra = prompt('Inserte una palabra').toLowerCase()
let vocales = 0
let consonantes = 0


for (const letra of palabra){

    if( letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u')
    vocales++
    else consonantes++

}
console.log(`La palabra ${palabra} tiene ${vocales} vocales, ${consonantes},consonantes Y ${palabra.length} letras`)


           