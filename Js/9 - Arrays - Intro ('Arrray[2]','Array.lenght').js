//#9 - Curso básico de JavaScript 2018 (Arrays - Introducción)

//Estructuras qe nos permiten agrupar varios datos
//Dentro se puede introducir cualquier "tipo de dato"
//Se declaran con []
//ueden declararse "vacios"
//Puden añadirse y eliminarse,cambiar de posicion elmentos dentro del mismo
//Cada elemento del "array" puede ser identificado pos su posicion dentro del mismo


let palabras = ['Muchacha',"♣","♠","♦","◙◙◙",666,true]
let numeros = [0,1,2,3,4,5,6,7]
let vacio = []

console.table(palabras)     //Tabla de todos los elementos del Array
console.log(palabras)       //Log 
console.log(palabras.length)
console.log(palabras[1],palabras[2],palabras[3]) // Posicion espeifica del array
console.log(`La Palabra ${palabras[0]} tiene, ${palabras[0].length} letras`) 

