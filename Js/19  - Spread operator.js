
//https://www.youtube.com/watch?v=5tKPYrtHHEk&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=23

// 'SPREAD' OPERATOR (OPERADOR DE EXPANSION)
// SINTAXIS : '...'


// ENVIAMOS LOS ELEMENTOS DEl ARRAY 'numeros' A LA FUNCION 'sumar(...numeros)' CON SPREAD OPERATOR

const numeros = [1,2,3,5,6,7]

const sumar = (a,b,c,) => {
    console.log((a+b+c) )

}

sumar(...numeros) // con '...' podemos separar los elementos del array Numeros' y enviarselos a la funcion 'sumar()'

//PUSH CON SPREAD OPERATOR los elementos de un array dntro de otro

const users = ['ebano','cachi','coco','sumatra']
const newUsers = ['calor','toto','tintin','bobmaginafriuuuta']
const newUsers2 = ['2','3','4']

for(pushed of newUsers){ // Recorro todo el array newUsers y los introduzco denro de 'users' atravez de 'pushed'
    users.push(pushed)
}


users.push(...newUsers2)//introduciomos todo el array 'newUsers2' dentro de users
console.log(users)

//COPIAR ARRAYS CON SPREAD

let aCopiar = ['me','van','a','copiar']
let recipiente = [2,'aCopiar copiado alado',...aCopiar]

console.log(recipiente)



//CONCATENAR ARRAYS

let concatenarAntiguo = aCopiar.concat(recipiente) // METODO DE CONCATENACION ANTIGUO CON EL METODO 'concat()'
console.log(concatenarAntiguo) 


let concatenados = [...aCopiar,...recipiente] //CONCATENADOS CON '...'
console.log(concatenados)


console.clear()

// 'PARAMETROS REST' CON SPREAD OPERATOR
// NOS PERMITEN ENVIAR O RECIBIR UN NUMERO INDEFINIDO DE PARAMETROS EN FORMA DE ARRAY

const restApi = (...rest)=> { // CREA UN ARRAY CON UNA CANTIDAD DE PARAMETROS SIN DEFINIR DENTRO
    console.log(rest)
} 

restApi() //AL EJECUTAR LA FUNCION SE EJECUTARN LOS PARAMETROS INCLUIDOS DENTRO DE 'restApi('esto se va a ejecutar')

// LIBRERIA 'MATH' CON SPREAD

let numerosMax = [2,3,4,77,8,9,0,0,4,77]

console.log(Math.max (...numerosMax))
console.log(Math.min (...numerosMax))

//ELIMINAR ELEMENTOS DUPLICADOS CON 'new'

const sinDuplicados = (...limpio)=>{  // CREO UNA CONSTANTE CON PARAMETROS INDEFINIDOS CON SPREAD (...limipo)

    console.log(new Set(...limpio))   // APLICO ELMETODO 'new' + SPREAD 'new Set (...limpio)' Y LO TRANSFORMAMOS A 'OBJETO' Y LUEGO A 'ARRAY' CON '...limpio'
                                      // ESTE METODO NO ADMITE PARAMETROS DUPLICADOS Y LOS ELIMINA
}                                     // LA FUNCION DEVUELVE EL ARRAY INTRODUCIDO SIN DUPLICADOS  


sinDuplicados(numerosMax)               // TODOS LOS ARRAY QUE INTRODUZCAMOS SERAN DEVUELTOS COMO 'OBJETOS; Y SIN DUPLICADOS