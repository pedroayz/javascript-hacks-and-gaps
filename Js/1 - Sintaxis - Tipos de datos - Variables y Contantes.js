 //#1 - Sintaxis - Tipos de datos - VAriables y Contantes - Curso básico de JavaScript 2018


 //Las 'Variables' son un espacio de memoria guardada para un futuro uso

 console.time() //Nos muestra en Msg el tiempo de ejecucion del Script
 let numero = 5.1265479524; //Numeros positivos
 let numeroNeg = -5.1265479524;//Numeros negativos
 let palabra = "Hola Mundo";// 
 let respuesta = true;  //booleano - Tipo de dato Logico
 respuesta = false;
 const Pi = 3.14160924

 console.log(numero)
 console.log(palabra + respuesta + Pi)
 console.log(respuesta)
 console.log(Pi)
 console.timeEnd() //Nos muestra en Msg el tiempo de ejecucion del Script