//#2 - Tipos de Operadores I - Curso básico de JavaScript 2018

/* Operadores Matematicos
+ Mas
- Menos
/ Dividir
* Multiplicar
% Modulo/Porcentaje

Operadores De Asignacion

= Igual
a += 3 (a=a+3) Suma y asignacion
a -= 3 (a=a-3) Resta y asignacion
a *= 3 (a=a*3) Multiplicacion y asignacion
a /= 3 (a=a/3) Divicion y asignacion
a %= 3 (a=a%3) Modulo y asignacion

Opeadores de Incremento & Decremento
 
a++ Post-Incremento
++a Pre-Incremento
a-- Post-Decremento
--a Pre-Decremento */

//Operadores Matematicos
let a = 5
let b = 2
let usuario = "Jhoan"

console.log(a+b)
console.log(a-b)
console.log(a*b)
console.log(a/b)
console.log(a%b)
console.log("Hola" +  usuario)

//Opeadores de Incremento & Decremento

console.log(a)
console.log(a++) // incrementa en uno la ultima devolucion de 'a'
console.log(++a)
console.log(a--)
console.log(--a)

//Operadores De Asignacion

a = a + 3 
a *= a * 3 
console.log(--a)


