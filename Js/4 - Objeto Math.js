//#4 - Objeto Math - Curso básico de JavaScript 2018

// "Math"
let number = 7.5
console.log(Math.E)               // Numero de "Euler"
console.log(Math.PI)              // Numero "Pi"
console.log(Math.abs(number))     // Valor absoluto
console.log(Math.ceil(number))    // Entero mayor mas probalbe
console.log(Math.floor(number))   // Entero menor mas probalbe
console.log(Math.pow(number,2))   // Devuelve la potencia de x elevado a la y "(number,2"
console.log(Math.random(number))  // Numero pseudo aleatorio entre 0 y 1  
console.log(Math.random(number)* 100) // Numero pseudo aleatorio entre 0 y 100  
console.log(Math.round(Math.random(50)* 100)) // Numero pseudo aleatorio entre 7 y 2  
console.log(Math.round(number))   // Redondeo Standar
console.log(Math.sign(number))    // Devuelve el singo de x, si es positivo,negativo o 0
console.log(Math.sqrt(9))          // Devuelve la raiz cuadrada de x