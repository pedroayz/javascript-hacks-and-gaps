
/* 
-- https://www.youtube.com/watch?v=21r6TEJh_Y0&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=19 
    
PROGRAMACION ORIENTADA A OBJEYOS  O 'POO'
    Es un nuevo 'paradigma de programacion' dentro de Java Script
   
    CONCEPTOS FUNDAMENTALES
    - CLASE
    - HERENCIA
    - OBJETO
    - METODO
    - EVENTO
    
*/

const guitarra = {

    Marca:'CucuMelo',
    Modelo:'AAC 42',
    Año:'1988',
    Precio:'$2400',
    Credito:'Todas',
    Contado:'-15%',
    Descuento:'Todas`{contado}`',
    Cuotas:['2','4','6','8','10','12'],
}

for(const key in guitarra){ // Con 'key' imprimimos todas las 'llaves' o claves del obejto
    console.log(key)
}
    console.log(guitarra)

// CLASS    

console.clear()

class Cadilac {

    constructor(marca,color,año,precio){ //La Palabra 'constructor' es una palabra reservada y es obligatoria para la funcion constructora
    this.marca = marca
    this.color = color
    this.año = año
    this.precio = precio

}    
}

const objetoCadilac = new Cadilac('Cadilac','Rojo','1976','21.000$')
console.log(objetoCadilac)
console.log(objetoCadilac.precio)

class Persona{
    
    constructor(nombre,apellido,edad){

        this.nombre = nombre
        this.apellido = apellido
        this.edad = edad
        this.datos = `Me llamo ${this.nombre} ${this.apellido} y tengo ${this.edad}`
    }

    saludar(){
        return `Hola me llamo ${this.nombre} y tengo ${this.edad}`
    }

}

const peter = new Persona('Peter','Aycaguer','31')
const silvia = new Persona('Silvia','Ponce','27')
console.log(peter)
console.log(peter.saludar())
console.log(silvia.saludar())