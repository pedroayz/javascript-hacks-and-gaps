
//https://www.youtube.com/watch?v=_aMg42Zqxa8&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=25

// 21 - Seleccionar elementos del DOM

//COMO ACCEDER A LOS ELEMENTOS O NODOS DEL 'DOM'

// METODOS

// 1 - document.getElementById() RETORNA LOS LEMENTOS CON EL ID INDICADO
// 2 - document.querySelector()  RETORNA EL PRIMER ELEMENTO PEDIDO
// 3 - document.querySelectorAll() RETORNA TODOS LOS ELEMENTOS PEDIDOS ENFORMA DE 'NODE LIST'

const button = document.getElementById('actionBttn') // GUARDAMOS DENTRO DE LA CONSTANTE 'button' EL ACCESO AL OBJETO CON ID 'actionBttn'

button.textContent = 'INCRUSTADO CON JS' // EDITAMOS LA PROPIEDAD textContent de 'button'

