//#3 - Strings - Metodos - Propiedades - Template Strings - Curso básico de JavaScript 2018

/* Metodo:todo lo que la cadena puede realizar 
Propiedades:Las que tra en si el "string" por ustipo de dato */

//Propiedades "LENGTH"

let cadena = "Calling a String"

console.log(cadena.length); // Nos devuelve el largo del "String"

//Metodo "toUpperCase" & "toLowerCase"

console.log(cadena.toUpperCase()); // DEvuelve el "String" en mayuscula

const cadenaMay = cadena.toUpperCase(); // Guardo el "String" en mayusculas dentro de la ocnstante "cadenaMay"
console.log(cadenaMay);
const cadenaMin = cadena.toLowerCase(); // Guardo el "String" en Minuscula dentro de la ocnstante "cadenaMay"
console.log(cadenaMin);

// Metodo "indexOf": DEvuelve la 1era concidencia dentro de la constante

console.log(cadena.indexOf("a"))
console.log(cadena.indexOf("Calling"))

// Metodo "replace": Remplaza A por B replace("A","B")

console.log(cadena.replace("Calling","Switching" ))

// Metodo "substring": Extrae los caracteres del principio al final si no agregamos el final [,"Opcional"]

console.log(cadena.substring(5 + 2))

// Metodo "Slice":Extrae los caracteres de la constante

console.log(cadena.slice(-5))   // Comienza a contar desde el final del objeto
console.log(cadena.slice(0,-5)) // Comienza desde el principio eh ignora los ultimos 5 "(0,-5)"" 

//Metodo "Trim": Quita lso espacios al principio y al final

let chain = ("        ....Pedro Ayçaguer.091046157.pedroayz@gmail.com.....              ");

console.log(chain)
console.log(chain.trim())


// ............... ------- Metodos de Ecma Script 6 -------- ..................


// Metodos "startsWith & endSWhit"
console.log(chain.startsWith(" "))  // Cheque con con caracter comienza el pedido y responde con "true or false"
console.log(chain.startsWith(" ",8)) // Le pedimos qe comienze luego del 8vo caracter)
console.log(chain.endsWith(" "))
console.log(chain.endsWith(" ",4)) //Cheque con con caracter FINALIZA el pedido dentro de los 1resos 4 "(" ",4))"" caracteres.
console.log(chain.endsWith("Pedro",4))


//Metodo "includes"

console.log(chain.includes("P"))//Averigua si el caracter ingresado es parte de la variable enviada
console.log(chain.includes("P",2,"@"))// comeinza a buscar desde la posicion 2 ("P","2")

//Metodo "repeat"
console.time() 
console.log(chain.repeat(5))// Repite la variable 2 veces ".repeat(2))"
console.log("me repito 5 veces / ".repeat(5))
console.timeEnd()

//Templeates Strings

let nombre = "Pedro "
let apellido = "Ayçaguer"
let edad = "31"

console.log("Hola! " +nombre+" "+apellido+" "+edad)

//PLANTILLAS LITERALES CON: " ` ` "

console.log(`HOLA ${nombre} ${apellido} Edad ${edad + 5 }`)
