
/* 
    FUNCIONES   
    Son fragmentos de codigo que definimos para recalizar una tarea y no velver a escribir el mismo codigo
    mas de una vez
    Esto no asyuda a 'MODULARIZAR EL CODIGO' - SEPARARLO EN FUNCIONES
    LAS FUNCIONES DEBENR REALIZAR UNA SOLA TAREA

*/
//-- https://www.youtube.com/watch?v=21r6TEJh_Y0&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=19 

/* 
    SINTAXIS ANTIGUA

    funcion nombreFuncion()
    {
    console.log(codigo a ejecutar)
    }

    SINTAXIS MODERNA

    const nombreFuncion = () => console.log(codigo a ejecutar)
   

*/

function saludar(){
    console.log('♪ ♪ ♫ ♫')
}
saludar()

const musical = () => console.log('♪ ♫ ♫ ►');
musical()

console.clear()


let number 
let number2 

const suma = (number,number2,number3) => number + number2 + number3
console.log(suma(Math.random(0)*15,1,2))

let resultado = suma(4,5,7) // GUARDAMOS LA FUNCION EN UNA VARIABLE PRONTA PARA EJECUTAR
console.log(resultado)
