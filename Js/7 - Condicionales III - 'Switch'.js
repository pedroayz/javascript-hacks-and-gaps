//#7 - Condicional switch

// Se utilliza para elegir 1no o mas entre distintos casos

//Sintaxis Simple

/* switch(evaluacion){

    case n1:
    break
    case n2 :
    break;
    case n3 :
    default:
}

//Sintaxis Multiple

switch(evaluacion){

    case n1:
    case n2:
    case n3:
    break;
    default:
} */


let num = Math.round(Math.random(0)*5)
switch(num)

{
    case 1:console.log("Numero Random = 1")
    break
    case 2:console.log("Numero Random = 2")
    break
    case 3:console.log("Numero Random = 3")
    break
    case 4:console.log("Numero Random = 4")
    break
    case 5:console.log("Numero Random = 5")
    break
    default: console.log("Te ha Tocado Cero ♥ ♫ ♫ ♪♪ ♥")
} 

switch(num)

{
    case 1:
    case 3:
    case 5:
    console.log(`${num} Es Impar`)
    break
    case 2:
    case 4:
    console.log(`${num} Es Par`)
    break
    default:console.log(`${num} Es 0..Quien + ♥ ♪ ♫ ♥ ?`)
}      

