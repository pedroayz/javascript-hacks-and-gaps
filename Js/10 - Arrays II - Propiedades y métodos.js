//10 - Arrays II - Propiedades y métodos


let palabras = ['Muchacha',"♣","♠","♦","◙◙◙",666,true]
let numeros = [666,0,1,2,3,4,5,6,7]
let vacio = []

// Propiedad 'length'

console.log(palabras.length)                                                  // Nos devuelve el largo en numeros del Array
console.log(`La Palabra ${palabras[0]} tiene, ${palabras[0].length} letras`) // Nos devuelve el largo de la posicion indeicada entre []


// Metodo 'Array.isArray'

let let1 = 4

console.log(Array.isArray(let1))      // Nos dice si ela variable es o no un 'Array'
console.log(Array.isArray(numeros))  //

// Metodos .shift() & .pop()


console.log(numeros.pop())            //Elimina el primer elemento del 'Array'
console.log(numeros.shift())         //Elimina el ultimo elemento del 'Array'
var Deleted = numeros.pop()         // Guardar elementos agregado o eliminados de una variable
console.log(Deleted)

// Metodos .push() & .unshift()

var added = numeros.push()        //Guarda los elementos guardados por '.push'
numeros.push(69)                 //Agrega el elemento al comienzo del 'Array'
numeros.unshift(69)             //Agrega el elemento al final del 'Array'
console.log(numeros) 


// Metodo .indexOf()

console.log(numeros.indexOf(69))       //Nos devuelve el numero de la posicion del  1er elemento nombrado en () dentro del array 
console.log(numeros.lastIndexOf(69))  //Nos devuelve el numero de la posicion del  Ultimo elemento nombrado en () dentro del array 


// Metodo .reverse()

console.log(numeros.reverse())       //Invierte el orden del 'Array'

// Metodo .join()


console.log(numeros.join())          //Separa cada elemento del 'Array' con el valor introducido dentro de ()
                                    //El valor por defecto son ','
console.log(numeros.join("♪ ♫ "))                                

console.clear()


// Metodo .splice(A,B,C) Cambia o eleimina los elementos dentro de 'Array' 

/* 
    Utiliza 3 parametros A,B,C
    A - Comienza a eliminar desde 'A'
    B - Elimina la cantidad de elementos nombrados en 'B'
    C - Son todos los elementos que se van a añadir luego de la segunda coma

*/

numeros.splice(3)                         //Elimina todos los elementos luego del parametro 1
numeros.splice(1,1)                      //Se posiciona en el valor A Y elimina todos los elementos indicados por el valor B 
numeros.splice(1,0,"Usando","splice")   //Se posiciona en A,elimina la cantidad de obejtos definidos en B, y agrega todos los que esten dsp de la seguna coma (C)
console.log(numeros)



// Metodo .slice(A,B) Extrae los elementos del 'Array' indeicados desde  "A" hasta "B".

let sliced = ['s','l','i','c','e','d']
console.log(sliced.slice(0,3))

// Si solo indecamos el paramatro A se posiciona en el mismo y copia el Array indicado apartir de este punto
console.log(sliced.slice(3))

// Si no existen estos parametros crea una copia del original
console.log(sliced.slice())






