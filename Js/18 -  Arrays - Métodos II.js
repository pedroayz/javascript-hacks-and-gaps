// ARRAYS METODOS II

//https://www.youtube.com/watch?v=Be4i1N09XzQ&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=22

// METODO 'FROM'- CONVIERTE EL ELEMENTO SELECCIONADO EN 'ARRAY' - EJ: .from(elementoSeleccionado)


const links = document.querySelectorAll('a') // INVOCA TODAS LAS ETIQUETAS 'a'DENTRO DEL DOM
let palabra = 'hola mundo'

console.log(Array.from(links)) // TRANSFORMA EL OBEJETO EN UN 'ARRAY'
console.log(palabra.split('')) 


// METODO 'SORT'-ORDENA LOS ELEMENTOS DENTR DE UN 'ARRAY' - EJ: .from(elementoSeleccionado)
//EJ: sort(callback) PUEDE RECIBIR O NO UN PARAMETRO ALGORIMTO PARA ORDENAR EL ARRAY

const words = ['ase','zsd','csdf','qsdf','esdf','fs','bf','e']
const numbers= ['45','2','1345','22','100']

console.log(words.sort()) // SE EJECUTA SIN PARAMETROS POR DEFECTO ORDENANANDO DE MENOR A MAYOR POR EL VALOR UNICODE DE CADA CARACTER
console.log(numbers.sort((a,b)=>b-a)) // SE EJECUTA DE MENOR A MAYOR CON EL CALLBACK INTRODUCIDO DENTRO DE LOS PARENTESIS DE .sort()


// METODO .forEach 


const forNumbers = [12,54,87,96,3,5,68]

forNumbers.forEach((forNumbers)=>console.log(forNumbers)) // MUESTRA TODOS LOS ELEMENTOS DEL ARRAY
forNumbers.forEach((forNumbers,index)=>console.log(`El numero ${forNumbers} esta en la posicion ${index}`))// MUESTRA LOS ELEMENTOS DEL ARAY Y SU NUMERO DE INDICE DEL MISMO

//METODOS .some() & .every()

//.some(callback) CHEQUEA SI ALGUNO DE LOS ELEMENTOS DEL ARRAY CUMPLEN LA CONDICION PROGRAMADA

console.log(words.some(words =>words.length>1)) // CHEQUE SI EL LARGO DE LOS ELEMENTOS FEL ARRAY ES SUPERIOR A 1


//.every(callback) CHEQUEA SI TODOS LOS ELEMENTOS DEL ARRAY CUMPLEN LA CONDICION PROGRAMADA

console.log(words.every(words =>words.length>0)) // CHEQUE SI EL LARGO DE TODOSLOS ELEMENTOS FEL ARRAY ES SUPERIOR A 1




// .map(callback) TRANSFORM TODOS LOS ELEMENTOS DEL ARRAY INTRODUCIDO Y DEVUELVE UNO NUEVO

console.log(forNumbers.map(forNumbers=>forNumbers*0))//MULTIPLICA TODOS LOS ELEMENTOS DEL ARRAY POR 0



let vacio = []
const multiplicar = forNumbers.map(forNumbers=> forNumbers* 3)

console.log(multiplicar)
vacio.push(multiplicar) // guardo el ARRAYMULTIPLICADO POR 'multiplicar' DENTRO  DEL ARRAY 'vacio'
console.log(vacio)


// .filter(callback) FILTRA TODOS LOS ELEMENTOS DEL ARRAY INTRODUCIDO Y DEVUELVE UNO NUEVO
console.clear()
let filtrado = forNumbers.filter(forNumbers=> forNumbers >= 40) // SOLO MUESTRA LOS NUMEROS SUPERIORES A 40
console.log(filtrado)

// reduce(callback) // TOMA TODOS LOS ELEMENTOS Y LOS REDUCE A UN UNICO VALOR

let reduceNumbers = forNumbers.reduce((a,b) => a+b) // SUMA TODOS LOS ELEMENTOS DEL ARRAY Y DEVUELVE UN UNICO VALOR
console.log(reduceNumbers)



const usuario =[
     {

    nombre:'claudio',
    genero:'hombre',
},
{

    nombre:'ana',
    genero:'mujer',
},
{

    nombre:'termas',
    genero:'mujer',
},
{

    nombre:'guga',
    genero:'mujer',
}

]


const usuariosMujeres = usuario.reduce((cont,usuario)=>

{

    if(usuario.genero == 'mujer')cont ++
    return cont
},)

console.log(usuariosMujeres)