
//https://www.youtube.com/watch?v=ZgomZF_Eaao&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=30
//26- DOM Traversing BUSQEDA DE OBJETOS EN EL DOM

//ELEMENTOS PADRE

/* ------------------ METODOS ----------------------------

-- parentNode -- Devuelve el nodo padre
-- parentElement -- devuelve el nodo elemento padre


 */


const parent = document.getElementById('Dos')

console.log(parent.parentNode)
console.log(parent.parentElement)

//HIJOS
/* 
 ------------------ METODOS ----------------------------
-childNodes - Devuelve todos los nodos hijos
-children - Devuelve todos los nodos elemento hijos
-firstChild - Devuelve el primer nodo hijos
-firstElementChild - Devuelve el primer nodo elemento hijos
-lastChild - Devuelve el ultimo nodo hijos 
-lastElementChild - Devuelve el ultimo nodo elemento hijos 
-LastElementChild
-hasChildNodes() - Devuelve true si el nodo tiene hijos y false si no los tiene

 */

 console.log(parent.childNodes)         // TODOS LOS NODOS INCLUEYENDO TEXTO Y SALTOD DE LINEA
 console.log(parent.children)            // SOLO NODOS ELEMENTO
 console.log(parent.children[2])         // SOLO EN NODO EN LA POSICION INDICADA EN 'children[2]'
 console.log(parent.firstChild)         // EL PRIMER NODO HIJO INCLUEYENDO TEXTO Y SALTOD DE LINEA
 console.log(parent.firstElementChild)  // EL PRIMER NODO HIJO 
 console.log(parent.lastChild)          // EL ULTIMO NODO HIJO INCLUEYENDO TEXTO Y SALTOD DE LINEA
 console.log(parent.lastElementChild)  // EL ULTIMO NODO HIJO 
 console.log(parent.hasChildNodes())   // retorna true o false dependiendo si el nodo tiene elementos hijos o no

 console.clear()

 //HERMANOS
/* 
 ------------------ METODOS ----------------------------
-nextSibling - Devuelve el siguiente nodo hermano
-nextElementSibling - Devuelve el siguiente nodo elemento hermano
-previousSibling - Devuelve el anterior nodo hermano
-previousElementSibling - Devuelve el anterior nodo elemento hermano
-children - Devuelve todos los nodos elemento hijos
-firstChild - Devuelve el primer nodo hijos
-firstElementChild - Devuelve el primer nodo elemento hijos
-lastChild - Devuelve el ultimo nodo hijos 
-lastElementChild - Devuelve el ultimo nodo elemento hijos 
-LastElementChild
-hasChildNodes() - Devuelve true si el nodo tiene hijos y false si no los tiene

 */

 console.log(parent.nextSibling)
 console.log(parent.nextElementSibling)
 console.log(parent.previousSibling)
 console.log(parent.previousElementSibling)