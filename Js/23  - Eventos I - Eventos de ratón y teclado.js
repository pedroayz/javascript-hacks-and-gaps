
//https://www.youtube.com/watch?v=vAw2pYQzG30&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=26

//23  - Eventos I - Eventos de ratón y teclado

/* 

Qes es un evento ? 
Cualqueir accion que sucede dentro de nuestro documento
EJ
-Contenido leido
-Contenido cargado
-movimiento del mous
-click del mouse
-tipoe de teclas en el teclado
-etc
*/

// METODO ANTIGUO DE INCENRCION DE EVENTOS

// -- <p onclick = saludo()> </p> FUNCION ON CLICK

let  getSaludo =  document.getElementById('TresUno')  
let  getSaludo2 =  document.getElementById('TresDos') 
let  getSaludo3 =  document.getElementById('TresTres') 

const saludo = () => {
    
    if(getSaludo.id = 'TresUno' ){
    console.log(getSaludo)
    getSaludo.style.transform = '2s'
    getSaludo.style.background = 'pink'
    getSaludo.style.height = '200px'
    getSaludo.style.opacity = '0.5'
    getSaludo.style.whidth = '1200px'
    getSaludo.style.marginRight = '600px'
    getSaludo.innerText = 'ME CLIKEASTE'

}
} 

// METODO UTILIZADO EN 'ANGULAR'
//<p (click)='saludo()' ></p>

// METODO MODERNO DE INCENRCION DE EVENTOS

/* METEODOS DEL RATON
- 'CLICK'       - SE ACTIVA CON UN CLICK
- 'DBLCLICK'    - SE ACTIVA CON DOBLE CLICK
- 'MOUSEENTER' - SE ACTIVA AL ENTRAR EN LA ZONA DEL EVENTO
- 'MOUSELEAVE' - SE ACTIVA AL ABANDONAR LA ZONA DEL EVENTO
- 'MOUSEDOWN' - SE ACTIVA CUANDO PULSAMOS EL BOTON IZQUIERDO DEL RATON
- 'MOUSEUP' - SE ACTIVA CUANDO SOLTAMOS EL CLICK DEL RATON
- 'MOUSEMOVE' - SE ACTIVA CUANDO MOVEMOS EL MOUSE DENTRO DE LA ZONA DEL EVENTO
*/


getSaludo.addEventListener('dblclick',() =>{ // - 'DBLCLICK'
    
    getSaludo.style.background = ''
    getSaludo.style.height = ''
    getSaludo.style.opacity = ''
    getSaludo.innerText = 'ME DOBLE CLICKEASTE'
    
})

console.log(getSaludo2)

getSaludo2.addEventListener('mouseenter',()=>{

    getSaludo2.style.background = 'pink'
    getSaludo2.style.height = '300px'
    getSaludo2.style.opacity = '0.9'
    getSaludo2.innerText = 'ME PUSISTE EL PUNTERO ARRIBA'
    
})


getSaludo2.addEventListener('mouseleave',()=>{

    getSaludo2.style.background = ''
    getSaludo2.style.height = ''
    getSaludo2.style.opacity = ''
    getSaludo2.innerText = 'ME SACASTE EL PUNTERO DE ARRIBA'
    getSaludo2.innerHTML = '<img src = https://scontent.fmvd1-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/47320955_112000933167310_4630272124328607744_o.jpg?_nc_cat=108&_nc_ohc=D_CAfrA5eOsAX98yf6_&_nc_ht=scontent.fmvd1-1.fna&oh=38d1e1b7f3bc91ad3845b30c63f53a6a&oe=5EB57E07>SACASTE EL PUNTERO RUMM!</button>'

})

getSaludo3.addEventListener('mousedown',()=>{

    getSaludo3.style.background = 'pink'
    getSaludo3.style.height = '300px'
    getSaludo3.style.opacity = '0.9'
    getSaludo3.innerText = 'ME DISTE CLICK DERECHO'

})

getSaludo3.addEventListener('mouseleave',()=>{

    getSaludo3.style = ''
    getSaludo3.style.transition = '5s'
    getSaludo3.innerText = 'YA NO SOY QUIEN ERA ANTES'

})

getSaludo3.addEventListener('mouseup',()=>{

   
    getSaludo3.innerText = 'me as soltao'

})

const getAction = document.getElementById('actionBttn')

getAction.addEventListener('mousemove',()=>{
    getAction.style.background = 'pink'
    getAction.style.height = '300px'
    getAction.style.opacity = '0.9'
    getAction.innerText = 'TE MUEVES ENCIMA MIO'


})

getAction.addEventListener('mouseleave',()=>{
    getAction.style = ''
    getAction.innerText = 'cha no'


})


//EVENTOS DEL TECLADO

/* 
- ' KEYUP'
- ' KEYDOWN'
- ' KEYPRESS'

*/

const getInput = document.getElementById('searchBar')

console.log(getInput)

getInput.addEventListener('keydown',()=>{

    console.log('pulsaste una tecla')
    getInput.style.background = ''
    getInput.style.height ='10%'


})

getInput.addEventListener('mouseleave',()=>{
    getInput.style = ''
    getInput.style.marginTop = '1%'


})

getInput.addEventListener('keyup',()=>{

  

})

getInput.addEventListener('keypress',()=>{

    getInput.style.opacity = '0.5'



    
})
