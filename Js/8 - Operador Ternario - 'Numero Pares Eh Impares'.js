//#8 - Curso básico de JS 2018 (Operador Ternario)


// ES Utilizado cuando la respuesta sera "True" o "False"

//Metodo Moderno
/* 
(num % 2 == 0) ? console.log(`${num} es par`):
                 console.log(`${num} es impar`);

//Metodo antiguo "if else"
if (num % 2 == 0) console.log(`${num} es par`);
else console.log(`${num} es impar`) */

//Metodo Moderno Varias sentencias


/* 
(num % 2 == 0) ? console.log(`${num} es par`):
                 console.log(`${num} es impar`); */

let num = Math.round(Math.random(0)*100);

(num % 2 == 0) ?
    (
        console.log(`${num} es par`)
       
    )
     :
    (
        console.log(`${num} es impar`)
    )

    



