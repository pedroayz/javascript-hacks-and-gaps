
//https://www.youtube.com/watch?v=UVPnw_APpAk&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=28

//23  - Eventos II - OBJETO EVENTO

const saludo = () =>{
    console.log('saludo')
}


const SendClick = document.getElementById('searchClick')
const SearchBar = document.getElementById('searchBar')
const GetButton = document.getElementById('Dos')


SendClick.addEventListener('click',(e)=>{
    console.log(e)
    
})

SendClick.addEventListener('submit',(e)=>{
    e.preventDefault() // IMPIDE QUE EL OBJETO SE COMPORTE POR DEFECTO, EN ESTE CASO LO UTILIZAMOS PARAQUE EL CLICK NO RECARGUE LA PAGINA LUEGO DE RES EJECUTADO
})

SearchBar.addEventListener('mouseenter',(e)=>{

    console.log(`X + Y de MouseEnter ${e.x} ${e.y}`)  

})

SearchBar.addEventListener('keyup',(e)=>{
    console.log(e.key)
    //console.log(e)   
    console.log(`Has pulsado la tecla ${e.key} su Key Code = ${e.keyCode} & Ctrl Key = ${e.ctrlKey}`)  

})

GetButton.addEventListener('click',(e)=>{

    console.log(e.target.innerText)
    e.target.classList.add('invisible')

})



