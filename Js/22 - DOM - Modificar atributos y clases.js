
//https://www.youtube.com/watch?v=vAw2pYQzG30&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=26

// 22 - DOM - Modificar atributos y clases

// ATRIBUTOS

    // 'element.getAtribute('atribute')


    const searchBar = document.getElementsByClassName('serach-bar')
    const searchId = document.getElementById('searchBar')


    console.log(searchId.type)

    //element.setAtribute('atribute')

    searchId.setAttribute('type','date') // CAMBIAMOS EL ATRYBUTO 'type' por 'date'

// INSERTAR O BORRAR CLASES


//CLASES

/*  element.classList.add('class','class')
    element.classList.remove('class','class)
    element.classList.toggle('class',[,class])
    element.classList.contain('class')
    element.classList.replace('OldClass','NewClass')
 */

 searchId.classList.add('dropdown-menu.show','remove')    // AGREGA UNA CLASE
 searchId.classList.remove('dropdown-menu.show')         // AGREGA UNA CLASE   
 console.log(searchId.classList.contains('serach-bar')) // PREGUNTA SI ELLEMENTO CONTIENELA CLASE INDICADA Y DEUELVE TRUE O FALSE
 searchId.classList.replace('remove','menu')            // REMUEVE EL PRIMER PARAMAETRO Y AGREGA EL SEGUNDO
 console.log(searchId)


 //LLAMANDO A LOS PARAMAETROS DETRO DE LOS OBJETOS

 console.log(searchId.id) // DEVUELVE EL ID DEL ELEMENTO RECUPERADO POR LA FUNCION 'searchId'
 console.log(searchId.value)