
// Un objeto es una estructura de datos que representan propiedads,valores y acciones que puede relizar el objeto
//Todoslos objetos tienen propiedades y atributos,comportamientos o acciones representados por pares de clave {CLave}:valor{value}
//https://www.youtube.com/watch?v=nO3SDEV3uJI&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=18



const ejemploMadre = {
    nombre:'Mama',
    genero:'femenio',
    año:1998,
    altura:1.35,
    ancho:1.20,
    hijos:['laura','diego','Rosa','Clauda','Angelica']
}
console.log(ejemploMadre.hijos)    //Invocando un objeto y sus propiedades
console.log(ejemploMadre.hijos[1])

for(const key in ejemploMadre){ // Con 'key' imprimimos todas las 'llaves' o claves del obejto

    console.log(key)
}

console.clear()

for(const key in ejemploMadre){ // Con (ejemploMadre[key]) imprimimos todas los atributis de las 'llaves' o claves del obejto

    console.log(ejemploMadre[key])
}



    console.log(ejemploMadre)
    console.log(`Hola ${ejemploMadre.nombre} tus hijos se llaman ${ejemploMadre.hijos.join('♫')}`)