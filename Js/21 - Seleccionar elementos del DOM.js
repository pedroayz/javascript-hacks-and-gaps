
//https://www.youtube.com/watch?v=_aMg42Zqxa8&list=PLROIqh_5RZeBAnmi0rqLkyZIAVmT5lZxG&index=25

// 21 - Seleccionar elementos del DOM

//COMO ACCEDER A LOS ELEMENTOS O NODOS DEL 'DOM'

// METODOS

// 1 - document.getElementById() RETORNA LOS LEMENTOS CON EL ID INDICADO
// 2 - document.querySelector()  RETORNA EL PRIMER ELEMENTO PEDIDO
// 3 - document.querySelectorAll() RETORNA TODOS LOS ELEMENTOS PEDIDOS ENFORMA DE 'NODE LIST'


// ' getELementById() '

const button = document.getElementById('actionBttn') // GUARDAMOS DENTRO DE LA CONSTANTE 'button' EL ACCESO AL OBJETO CON ID 'actionBttn'

button.textContent = 'INCRUSTADO CON JS' // EDITAMOS LA PROPIEDAD textContent de 'button'

// querySelector() & querySelectorAll()

document.querySelector('.list-group-item')  // A QUERY SESLECTOR DEBEMOS ENVIARLE UN SELECTOR DE CSS COMO .CLASE O #ID
document.querySelector('div:nth-child(1)') // PODEMOS ACCEDER A LAS PSEDUO-CLASES COMO 'nth:child()'
document.querySelectorAll('.list-group-item') 

// querySelector puede ser invocado desdee cualquier elemento no solo desde 'document'
// ejemplo element.querySElector()
console.clear()
const span = document.querySelector('.btn-group')

console.log(span.textContent)

const selectorAll = document.querySelectorAll('.list-group-item')
//selectorAll[3,2].style.color = 'red'

// QUERY SELECTOR + SPREAD OPERATOR
// ESTAOPCION NO TIENE UN SOPORTE TOTAL EN LOS NAVEGADORES

const selectorSpread =
[...document.querySelectorAll('.list-group-item')]// TRANSFORMAMOS EL NODE LIST QUE DEVUELVE QUERY SELECTOR EN UN ARRAY

console.log(selectorAll)

selectorSpread.map(sel=>sel.style.color = 'white')
selectorSpread.map(sel=>sel.style.background = 'WHITE')
selectorSpread.map(sel=>sel.style.width = '100%')

const selectorSpreadA =
[...document.querySelectorAll('a')]

selectorSpreadA.map(sel=>sel.style.width = '350px')
selectorSpreadA.map(sel=>sel.style.background = 'white')
selectorSpreadA.map(sel=>sel.style.color = 'black')


// QUERY SELECTOR + ARRAY
// ESTA OPCION PARA TRANSFORMAR EL NODE LIST EN ARRAY TIENE 100% DE SOPORTE EN TODOS LOS NAVEGADORES
const selectorArray = Array.from(document.querySelectorAll('.list-group-item'))

selectorArray.map(sel=>sel.style.color = 'BLACK')



